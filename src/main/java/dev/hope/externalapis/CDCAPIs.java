package dev.hope.externalapis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.hope.models.CDCVaccinationData;
import dev.hope.models.Placeholder;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class CDCAPIs {

    private static final String vaccinationURL = "https://covid.cdc.gov/covid-data-tracker/COVIDData/getAjaxData?id=vaccination_data";
    private static final ObjectMapper objectMapper = new ObjectMapper();


    public static void getAllVaccinationData(){
        objectMapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpResponse<String> response = Unirest.get(vaccinationURL).asString();
        try {
            Placeholder ph = objectMapper.readValue(response.getBody(),Placeholder.class);
            CDCVaccinationData[] vData = ph.getVaccinationData();
            for(CDCVaccinationData dataSet: vData){
                System.out.println(dataSet.toString());
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
