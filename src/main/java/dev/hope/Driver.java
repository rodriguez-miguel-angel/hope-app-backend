package dev.hope;

import dev.hope.externalapis.CDCAPIs;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Driver {

    public static void main(String[] args) {
        CDCAPIs.getAllVaccinationData();
//        SpringApplication.run(Driver.class,args);
    }
}
