package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Objects;

public class Placeholder {
    private String runid;
    @JsonProperty("vaccination_data")
    private CDCVaccinationData[] vaccinationData;

    public Placeholder() {
    }

    public String getRunid() {
        return runid;
    }

    public void setRunid(String runid) {
        this.runid = runid;
    }

    public CDCVaccinationData[] getVaccinationData() {
        return vaccinationData;
    }

    public void setVaccinationData(CDCVaccinationData[] vaccinationData) {
        this.vaccinationData = vaccinationData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Placeholder that = (Placeholder) o;
        return Objects.equals(runid, that.runid) && Arrays.equals(vaccinationData, that.vaccinationData);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(runid);
        result = 31 * result + Arrays.hashCode(vaccinationData);
        return result;
    }

    @Override
    public String toString() {
        return "Placeholder{" +
                "runid='" + runid + '\'' +
                ", vaccinationData=" + Arrays.toString(vaccinationData) +
                '}';
    }
}
